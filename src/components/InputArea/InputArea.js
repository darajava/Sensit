import React from 'react';

import CSSModules from 'react-css-modules';
import styles from './styles.css';
import Glyphicon from 'react-bootstrap/lib/Glyphicon';
import {returnRich} from '../Message/Message';
import TakePhoto from '../TakePhoto/TakePhoto';
import ReactDOMServer from 'react-dom/server';


const InputArea = (props) => {

  let sendMessage = (e) => {
    console.log('hihi')

    if (e.which === 13 || typeof e.which === 'undefined') {
      e.preventDefault();

      props.sendMessage(document.getElementById('message-box').innerHTML, !!props.sensit);
      document.getElementById('message-box').innerHTML = '';
      props.typing(false);
    } 

    document.getElementById('message-box').focus();
  };

  let sendTyping = () => {
    props.typing(true);
  }

  let toggleSensit = (e) => {
    if (props.sensit) {
      props.showSensit()
    } else {
      props.hideSensit()
    }

    document.getElementById('message-box').focus();

    e.stopPropagation();
    e.preventDefault();
  }

  return (
    <div styleName="bottom" id="seen">

      <div styleName={"input-holder " + (props.sensit ? 'sensitive' : '')}>
        <span
          id="message-box"
          styleName={"main-input " }
          placeholder="Type your message..."
          onKeyDown={sendMessage}
          onKeyUp={sendTyping}
          contentEditable="true">

        </span>
        <div styleName="right">
          <span onClick={toggleSensit} styleName={"sensit-button " + (props.sensit ? "on" : "")}>
            <Glyphicon glyph="lock" />
          </span>
          <span>
            <TakePhoto sendPhoto={props.sendPhoto}/>
          </span>
        </div>
      </div>
      <div styleName="button-container">
        <button styleName={"send-select " + (props.sensit ? "sensitive lock" : "")} onClick={(e) => sendMessage(e)} >
          <span styleName={"glyph-position"} >
            <Glyphicon glyph={props.sensit ? "" : "send"} />
          </span>
        </button>
      </div>
    </div>
  )
}

export default CSSModules(InputArea, styles, {allowMultiple: true});
 