import React from 'react';
import ReactDOM from 'react-dom';
import {AppContainer} from 'react-hot-loader';
import App from './App';

const setFcmToken = (token) => {
  if (!localStorage.getItem('token') || !localStorage.getItem('token').length) return;

  fetch("http://" + process.env.REACT_APP_API_URL + "/fcm-token", {
    method: "POST",

    headers: {
      "Content-Type": "application/json",
      "Authorization": "Bearer " + localStorage.getItem('token'),
    },

    body: JSON.stringify({
      'userId': localStorage.getItem('id'),
      fcmToken: token,
    }),
  });
}

const initFirebase = () => {
  if (!window.FirebasePlugin) return;

  getToken();

  // Get notified when a token is refreshed
  window.FirebasePlugin.onTokenRefresh(function(token) {
    // save this server-side and use it to push notifications to this device
    setFcmToken(token);
  }, function(error) {
    alert(error);
  });

  // Get notified when the user opens a notification
  window.FirebasePlugin.onNotificationOpen(function(notification) {
    // console.log(JSON.stringify(notification));
  }, function(error) {
    console.error(error);
  });    
}

const getToken = () => {
  if (!window.FirebasePlugin) return;
  
  window.FirebasePlugin.getToken(function(token) {
    // save this server-side and use it to push notifications to this device
    setFcmToken(token);
  }, function(error) {
    console.error(error);
  });
}

window.updatePushToken = () => {
  getToken();
}

const startApp = () => {
  const rootEl = document.getElementById('root');

  initFirebase();

  ReactDOM.render(
    <AppContainer>
      <App />
    </AppContainer>,
    rootEl
  );

  if (module.hot) {
    module.hot.accept('./App', () => {
      const NextApp = require('./App').default; // eslint-disable-line global-require
      ReactDOM.render(
        <AppContainer>
          <NextApp />
        </AppContainer>,
        rootEl
      );
    });
  }
};

if (window.cordova) {
  document.addEventListener('deviceready', () => {
    startApp();
  }, false);
} else {
  startApp();
}
