# Sensit

## Demo
Web client: http://darajava.ie/sensit (please use Chrome)

Android app: https://play.google.com/store/apps/details?id=ie.sensit.sensit

There are 4 users on the demo: `mary`, `dave`, `john` and `rebecca` (all case sensitive). The password for all users is simply `a`. PIN is `1234`.

This is very much beta so there are bound to be some bugs. Sometimes the messages don't load at all and it requires an app restart, looking into this. 

(todo: get https)

## Screenshots

### Web
<img src="https://i.imgur.com/9QWU2WL.png" width="800px"/>

### Android
<img src="https://i.imgur.com/RkRQv1U.jpg" width="320px"/>


### iOS

TODO

## Build

Clone the repo

```bash
npm i

# To test locally (debug, unminified version)
npm run start:[staging|prelive|production]

# To build
npm run build:[staging|prelive|production]
```

The different environments correspond to `.env.{ENV}` files stored in the root which contain the API URLs. Building creates a bundle in `./www` with an `index.html` root. See `package.json` for details. 

## Deploy

### Android

Follow instructions in https://gitlab.com/darajava/Sensit-cordova

### Web 

Currently move output of `npm run build:prelive` to `/var/www/html/sensit`.

## Backend

See https://gitlab.com/darajava/Sensit-server for setup of the chat server, the API server and the database.

